import './App.css';
import React from 'react';
import WebToonList from "./pages/WebToonList";
import WebToonWishList from "./pages/WebToonWishList";
import {Routes, Route} from "react-router-dom";
import DefaultLayout from "./layouts/DefaultLayout";

function App() {
  return (
    <div className="App">
        <Routes>
            <Route path="/" element= {<DefaultLayout><WebToonList /></DefaultLayout>} />
            <Route path="/wish" element={<DefaultLayout><WebToonWishList /> </DefaultLayout>} />
        </Routes>
    </div>
  );
}

export default App;
