import React, {useEffect, useState} from 'react';

const WebToonList = () => {
    const [webToons, setWebToons] = useState([])
    const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || [])

    useEffect(() => {
        fetch('http://localhost:3000/webtoons.json')
            .then(response => response.json())
            .then(data => setWebToons(data))
    }, [])

    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites))
    }, [favorites]);

    const addToFavorite = (webToon) => {
        if (favorites.find(fav => fav.id === webToon.id)) {
            setFavorites(favorites.filter(x => x.id !== webToon.id));
        } else {
            setFavorites([...favorites, webToon]);
        }

        // 병행제어 기법 -> 멀티쓰레드 제어

    }
    return (
        <div>
            <h1>웹툰 목록</h1>
            {webToons.map(webToon => (
                <div key={webToon.id}>
                    <img src={process.env.PUBLIC_URL + '/assets/' + webToon.imgUrl} width='150px'/>
                    <p>{webToon.webtoonName}</p>
                     <button onClick={() => addToFavorite(webToon)}> {favorites.find(fav => fav.id === webToon.id) ? '♥' : '♡'} </button>
                </div>
            ))}
        </div>
    )
}

export default WebToonList;