import React from "react";

const DefaultLayout = ({ children }) => {
    return (
        <>
            <div>헤더</div>
            <main>{children}</main>
            <footer>footer</footer>
        </>
    )
}

export default DefaultLayout;